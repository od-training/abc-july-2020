import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { Video } from './dashboard/types';

@Injectable({
  providedIn: 'root',
})
export class VideoLoaderService {
  constructor(private http: HttpClient) {}

  loadVideos() {
    return this.http
      .get<Video[]>('https://api.angularbootcamp.com/videos')
      .pipe(
        map((videos) =>
          videos.map((e) => ({ ...e, author: e.author.toUpperCase() }))
        )
      );
  }
}
