import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css'],
})
export class StatFiltersComponent implements OnInit {
  form: FormGroup;

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      date: [''],
      status: [''],
    });
  }

  ngOnInit(): void {}

  logTheForm() {
    console.log(this.form.value);
  }
}
