import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Video } from '../types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css'],
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[] | undefined;
  @Input() selectedVideo: Video | undefined;
  @Output() chooseVideo = new EventEmitter<Video>();

  constructor() {}

  ngOnInit(): void {}

  selectVideo(video: Video) {
    this.chooseVideo.emit(video);
  }
}
