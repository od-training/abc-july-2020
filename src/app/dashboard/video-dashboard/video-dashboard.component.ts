import { Component, OnInit } from '@angular/core';
import { Video } from '../types';
import { VideoLoaderService } from 'src/app/video-loader.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css'],
})
export class VideoDashboardComponent implements OnInit {
  videoList: Observable<Video[]>;
  currentVideo: Video | undefined;

  constructor(svc: VideoLoaderService) {
    this.videoList = svc.loadVideos();
  }

  ngOnInit(): void {}

  setSelectedVideo(video: Video) {
    this.currentVideo = video;
  }
}
